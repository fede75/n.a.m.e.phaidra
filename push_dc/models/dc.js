// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// Schema
var dcSchema = new mongoose.Schema({
    dc: String
    
});

// Return model
module.exports = restful.model('dublincores', dcSchema);

