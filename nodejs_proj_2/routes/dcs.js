var mongo = require('mongodb');

var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new Db('phaidradcdatabase3', server);

db.open(function(err, db) {
    if(!err) {
        console.log("Connected to phaidradcdatabase ");
        db.collection('dcs', {strict:true}, function(err, collection) {
            if (err) {
                console.log("Creazione del database di dublin core phaidra");
                populateDB();
            }
        });
    }
});

exports.findById = function(req, res) {
    var id = req.params.id;
    console.log('Retrieving dc: ' + id);
    db.collection('dcs', function(err, collection) {
        collection.findOne({'_id':new BSON.ObjectID(id)}, function(err, item) {
            res.send(item);
        });
    });
};

exports.findAll = function(req, res) {
    db.collection('dcs', function(err, collection) {
        collection.find().toArray(function(err, items) {
            res.send(items);
        });
    });
};

exports.adddc = function(req, res) {
    var dc = req.body;
    console.log('Adding dc: ' + JSON.stringify(dc));
    db.collection('dcs', function(err, collection) {
        collection.insert(dc, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred'});
            } else {
                console.log('Success: ' + JSON.stringify(result[0]));
                res.send(result[0]);
            }
        });
    });
}

exports.updatedc = function(req, res) {
    var id = req.params.id;
    var dc = req.body;
    console.log('Updating dc: ' + id);
    console.log(JSON.stringify(dc));
    db.collection('dcs', function(err, collection) {
        collection.update({'_id':new BSON.ObjectID(id)}, dc, {safe:true}, function(err, result) {
            if (err) {
                console.log('Error updating dc: ' + err);
                res.send({'error':'An error has occurred'});
            } else {
                console.log('' + result + ' document(s) updated');
                res.send(dc);
            }
        });
    });
}

exports.deletedc = function(req, res) {
    var id = req.params.id;
    console.log('Deleting dc: ' + id);
    db.collection('dcs', function(err, collection) {
        collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(req.body);
            }
        });
    });
}

/*--------------------------------------------------------------------------------------------------------------------*/

var populateDB = function() {

    
	
	
	
	
	
	var dcs = [
	
	{"metadata":{"alerts":[],"dc":[{"ui_value":"All rights reserved","xmlname":"rights"},{"ui_value":"ita","xmlname":"language"},{"ui_value":"Gian Girolamo, Z. (Zannichelli)","xmlname":"creator"},{"ui_value":"2016-04-07T12:04:02.499Z","xmlname":"date"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"piante, lidi, Venezia, Zannichelli, tavole, illustrazioni, colori","xmlname":"subject"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"In Venezia : appresso Antonio Bortoli, 1735.\nTrattasi di parte delle tavole originali per il libro &quot;Istoria delle piante che nascono ne’ lidi intorno a Venezia ..&quot; Opera postuma","xmlname":"description"},{"ui_value":"http:\/\/phaidradev.cab.unipd.it\/o:61439","xmlname":"identifier"},{"ui_value":"DEFAULT","xmlname":"format"},{"ui_value":"158798 bytes","xmlname":"format"},{"ui_value":"Page","xmlname":"type"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"Tavole originali del libro &quot;Istoria delle piante che nascono ne’ lidi intorno a Venezia ..&quot;","xmlname":"title"},{"ui_value":"Bortoli, Antonio","xmlname":"publisher"}],"status":200}},
	{"metadata":{"alerts":[],"dc":[{"ublalallalalalalalalalalame":"rights"},{"ui_value":"ita","xmlname":"language"},{"ui_value":"Gian Girolamo, Z. (Zannichelli)","xmlname":"creator"},{"ui_value":"2016-04-07T12:04:02.499Z","xmlname":"date"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"piante, lidi, Venezia, Zannichelli, tavole, illustrazioni, colori","xmlname":"subject"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"In Venezia : appresso Antonio Bortoli, 1735.\nTrattasi di parte delle tavole originali per il libro &quot;Istoria delle piante che nascono ne’ lidi intorno a Venezia ..&quot; Opera postuma","xmlname":"description"},{"ui_value":"http:\/\/phaidradev.cab.unipd.it\/o:61439","xmlname":"identifier"},{"ui_value":"DEFAULT","xmlname":"format"},{"ui_value":"158798 bytes","xmlname":"format"},{"ui_value":"Page","xmlname":"type"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"Tavole originali del libro &quot;Istoria delle piante che nascono ne’ lidi intorno a Venezia ..&quot;","xmlname":"title"},{"ui_value":"Bortoli, Antonio","xmlname":"publisher"}],"status":200}}
	
	
	
    
    ];

    db.collection('dcs', function(err, collection) {
        collection.insert(dcs, {safe:true}, function(err, result) {});
    });

};