var mongo = require('mongodb');

var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new Db('dublincoredb', server);

db.open(function(err, db) {
    if(!err) {
        console.log("Connected to 'dublincoredb' database");
        db.collection('dublincores', {strict:true}, function(err, collection) {
            if (err) {
                console.log("The ...");
                populateDB();
            }
        });
    }
});

exports.findById = function(req, res) {
    var id = req.params.id;
    console.log('Retrieving dublincore: ' + id);
    db.collection('dublincores', function(err, collection) {
        collection.findOne({'_id':new BSON.ObjectID(id)}, function(err, item) {
            res.send(item);
        });
    });
};

exports.findAll = function(req, res) {
    db.collection('dublincores', function(err, collection) {
        collection.find().toArray(function(err, items) {
            res.send(items);
        });
    });
};

exports.addDublincore = function(req, res) {
    var dublincore = req.body;
    console.log('Adding dublincore: ' + JSON.stringify(dublincore));
    db.collection('dublincores', function(err, collection) {
        collection.insert(dublincore, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred'});
            } else {
                console.log('Success: ' + JSON.stringify(result[0]));
                res.send(result[0]);
            }
        });
    });
}

exports.updateDublincore = function(req, res) {
    var id = req.params.id;
    var dublincore = req.body;
    console.log('Updating dublincore: ' + id);
    console.log(JSON.stringify(dublincore));
    db.collection('dublincores', function(err, collection) {
        collection.update({'_id':new BSON.ObjectID(id)}, dublincore, {safe:true}, function(err, result) {
            if (err) {
                console.log('Error updating dublincore: ' + err);
                res.send({'error':'An error has occurred'});
            } else {
                console.log('' + result + ' document(s) updated');
                res.send(dublincore);
            }
        });
    });
}

exports.deleteDublincore = function(req, res) {
    var id = req.params.id;
    console.log('Deleting dublincore: ' + id);
    db.collection('dublincores', function(err, collection) {
        collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(req.body);
            }
        });
    });
}



var populateDB = function() {

    var dublincores = [
    
	{"metadata":{"alerts":[],"dc":[{"ui_value":"All rights reserved","xmlname":"rights"},{"ui_value":"ita","xmlname":"language"},{"ui_value":"Gian Girolamo, Z. (Zannichelli)","xmlname":"creator"},{"ui_value":"2016-04-07T12:04:02.499Z","xmlname":"date"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"piante, lidi, Venezia, Zannichelli, tavole, illustrazioni, colori","xmlname":"subject"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"In Venezia : appresso Antonio Bortoli, 1735.\nTrattasi di parte delle tavole originali per il libro &quot;Istoria delle piante che nascono ne� lidi intorno a Venezia ..&quot; Opera postuma","xmlname":"description"},{"ui_value":"http:\/\/phaidradev.cab.unipd.it\/o:61439","xmlname":"identifier"},{"ui_value":"DEFAULT","xmlname":"format"},{"ui_value":"158798 bytes","xmlname":"format"},{"ui_value":"Page","xmlname":"type"},{"attributes":[{"ui_value":"ita","xmlname":"xml:lang"}],"ui_value":"Tavole originali del libro &quot;Istoria delle piante che nascono ne� lidi intorno a Venezia ..&quot;","xmlname":"title"},{"ui_value":"Bortoli, Antonio","xmlname":"publisher"}],"status":200}}
		
	
	
	];

    db.collection('dublincores', function(err, collection) {
        collection.insert(dublincores, {safe:true}, function(err, result) {});
    });

};