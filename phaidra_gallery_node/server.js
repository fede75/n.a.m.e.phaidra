var express = require('express'),
    dublincore = require('./routes/dublincores');

var app = express();

app.configure(function () {
    app.use(express.logger('dev'));     
    app.use(express.bodyParser());
});

app.get('/dublincores', dublincore.findAll);
app.get('/dublincores/:id', dublincore.findById);
app.post('/dublincores', dublincore.addDublincore);
app.put('/dublincores/:id', dublincore.updateDublincore);
app.delete('/dublincores/:id', dublincore.deleteDublincore);

app.listen(3000);
console.log('Listening on port 3000...');