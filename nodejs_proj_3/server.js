

var express = require('express'),
    dc = require('./routes/dcs');

var app = express();

app.configure(function () {
    app.use(express.logger('dev'));     /* 'default', 'short', 'tiny', 'dev' */
    app.use(express.bodyParser());
});

app.get('/dcs', dc.findAll);
app.get('/dcs/:id', dc.findById);
app.post('/dcs', dc.adddc);
app.put('/dcs/:id', dc.updatedc);
app.delete('/dcs/:id', dc.deletedc);






app.listen(3000);
console.log('Ok, sei connesso al server...');






