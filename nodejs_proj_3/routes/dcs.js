var mongo = require('mongodb');
var request = require('request');
var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new Db('phaidradcdatabase4', server);





db.open(function(err, db) {
    if(!err) {
        console.log("Connected to phaidradcdatabase ");
        db.collection('dcs', {strict:true}, function(err, collection) {
            if (err) {
                console.log("Creazione del database di dublin core phaidra");
                populateDB();
            }
        });
    }
});

exports.findById = function(req, res) {
    var id = req.params.id;
    console.log('Retrieving dc: ' + id);
    db.collection('dcs', function(err, collection) {
        collection.findOne({'_id':new BSON.ObjectID(id)}, function(err, item) {
            res.send(item);
        });
    });
};

exports.findAll = function(req, res) {
    db.collection('dcs', function(err, collection) {
        collection.find().toArray(function(err, items) {
            res.send(items);
        });
    });
};

exports.adddc = function(req, res) {
    var dc = req.body;
    console.log('Adding dc: ' + JSON.stringify(dc));
	console.log(dc);
    db.collection('dcs', function(err, collection) {
        collection.insert(dc, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred'});
            } else {
                console.log('Success: ' + JSON.stringify(result[0]));
                res.send(result[0]);
            }
        });
    });
}

exports.updatedc = function(req, res) {
    var id = req.params.id;
    var dc = req.body;
    console.log('Updating dc: ' + id);
    console.log(JSON.stringify(dc));
    db.collection('dcs', function(err, collection) {
        collection.update({'_id':new BSON.ObjectID(id)}, dc, {safe:true}, function(err, result) {
            if (err) {
                console.log('Error updating dc: ' + err);
                res.send({'error':'An error has occurred'});
            } else {
                console.log('' + result + ' document(s) updated');
                res.send(dc);
            }
        });
    });
}

exports.deletedc = function(req, res) {
    var id = req.params.id;
    console.log('Deleting dc: ' + id);
    db.collection('dcs', function(err, collection) {
        collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(req.body);
            }
        });
    });
}





var populateDB = function() {

    
  request('https://phaidradev.cab.unipd.it/api/object/o:61439/dc', function (error, response, body) {
  console.log('error:', error); 
  console.log('statusCode:', response && response.statusCode); 
  console.log('body:', body); 
  var data=JSON.parse(body);
  var dcs=data.metadata.dc;
  db.collection('dcs', function(err, collection) {
        collection.insert(dcs, {safe:true}, function(err, result) {});
		 });
  
});





	
	
	
	

};