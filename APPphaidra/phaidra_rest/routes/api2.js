// Dependencies
var express = require('express');
var router = express.Router();

// Models
var Product = require('../models/dublincore');

// Routes
Product.methods(['get', 'put', 'post', 'delete']);
Product.register(router, '/dublincores');

// Return router
module.exports = router;