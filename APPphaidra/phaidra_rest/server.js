// Dipendenze
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

// Connessione a MongoDB
mongoose.connect('mongodb://localhost/rest_test');

// Server Express
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Routes
app.use('/api', require('./routes/api2'));

// Start server
app.listen(3000);
console.log('in attesa su porta 3000...');


