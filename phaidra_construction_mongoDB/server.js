    //  SERVER.JS

    
    var express  = require('express');
    var app      = express();                              
    var mongoose = require('mongoose');                   
    var morgan = require('morgan');             
    var bodyParser = require('body-parser');   
    var methodOverride = require('method-override'); 

    
   	// CONFIGURAZIONE

    mongoose.connect('mongodb://fede75:Vallisneri_1661@ds153400.mlab.com:53400/phaidracollections/');   

    app.use(express.static(__dirname + '/public'));                
    app.use(morgan('dev'));                                         
    app.use(bodyParser.urlencoded({'extended':'true'}));            
    app.use(bodyParser.json());                                     
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 
    app.use(methodOverride());
	
	
	
	
     // MODELLO 
    var Phaidracollection = mongoose.model('Phaidracollection', {
        text : String
    });
	
		

     // ROUTES

   
    app.get('/api/phaidracollections', function(req, res) {

       
        Phaidracollection.find(function(err, phaidracollections) {

            
            if (err)
                res.send(err)

            res.json(phaidracollections); 
        });
    });

    
    app.post('/api/phaidracollections', function(req, res) {
console.log("prova");
        
        Phaidracollection.create({
            text : JSON.stringify(req.body),
            done : false
        }, function(err, phaidracollection) {
            if (err)
                res.send(err);

            
            Phaidracollection.find(function(err, phaidracollections) {
                if (err)
                    res.send(err)
                res.json(phaidracollections);
            });
        });

    });

    
    app.delete('/api/phaidracollections/:phaidracollection_id', function(req, res) {
        Phaidracollection.remove({
            _id : req.params.phaidracollection_id
        }, function(err, phaidracollection) {
            if (err)
                res.send(err);

            
            Phaidracollection.find(function(err, phaidracollections) {
                if (err)
                    res.send(err)
                res.json(phaidracollections);
            });
        });
    });
	
	
	
	
	
	
	
	 //APPLICAZIONE
	 
    app.get('*', function(req, res) {
        res.sendfile('./public/index.html'); 
    });
	
	

	
	
	

    // LISTEN
    app.listen(8080);
    console.log("In ascolto in porta 8080");
