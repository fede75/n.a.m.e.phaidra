var scotchPhaidracollection = angular.module('scotchPhaidracollection', []);

function mainController($scope, $http) {
    $scope.formData = {};

    
    $http.get('/api/phaidracollections')
        .success(function(data) {
            $scope.Phaidracollections = data;
            console.log(data);
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });


    $scope.createPhaidracollection = function() {


	
		$http.get('https://phaidradev.cab.unipd.it/api/object/o:61439/dc')
        .success(function(data) {
            console.log('>>>>>>'+data);
			
			var req = {
				 method: 'POST',
				 url: '/api/Phaidracollections',
				 headers: {
				   'Content-Type': 'application/json'
				 },
				 data: JSON.stringify(data)
				};			
			
			$http(req)
            .success(function(data) {
                $scope.formData = {}; 
                $scope.Phaidracollections = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });
		

    };

    
    $scope.deletePhaidracollection = function(id) {
        $http.delete('/api/Phaidracollections/' + id)
            .success(function(data) {
                $scope.Phaidracollections = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

}